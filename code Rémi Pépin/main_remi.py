from settings import *
from scraping import *
from exploration import *
import nettoyage_remi


listeCSV = os.listdir("RepertoireCSV")
dataFrames = []
for csv in listeCSV:
    dataFrames.append(creation_dataFrame(csv))




def comparaison_temps_remi(dataFrame, liste_n=[1,10, 100, 500]):
    temps_classique = []
    temps_swifter = []
    temps_parallelized_hand_made = []
    for n in liste_n:
        df=pd.DataFrame(dataFrame.iloc[:n])
        start_time1 = time.time()
        nettoyage_remi.nettoyage(df)
        end_time1 = time.time()
        temps_classique.append(end_time1 - start_time1)
        print(f'Temps base pour traiter {n} lignes : {end_time1 - start_time1}')
        start_time_fast = time.time()
        nettoyage_remi.swifter_clean(df)
        end_time_fast = time.time()
        temps_swifter.append(end_time_fast - start_time_fast)
        print(f'Temps base swifter pour traiter {n} lignes : {end_time_fast - start_time_fast}')
        start_time3 = time.time()
        nettoyage_remi.parallelize_dataframe(df, nettoyage_remi.nettoyage)
        end_time3 = time.time()
        temps_parallelized_hand_made.append(end_time3 - start_time3)
        print(f'Temps base parrallèle pour traiter {n} lignes : {end_time3 - start_time3}')
    print(temps_classique)
    print(temps_swifter)
    print(temps_parallelized_hand_made)
    plt.plot(liste_n, temps_classique, label="Lemmatisation")
    plt.plot(liste_n, temps_swifter, label="fast")
    plt.plot(liste_n, temps_parallelized_hand_made, label="parallelization")
    plt.title("Comparaison de la lemmatisation et de la stemmatisation")
    plt.xlabel("Nombre de lignes")
    plt.ylabel("Temps en secondes")
    plt.legend()
    plt.show()

###### Lemmatisation #######
if __name__ == '__main__':
    comparaison_temps_remi(dataFrames[1], list(range(1000, 5001,1000)))
