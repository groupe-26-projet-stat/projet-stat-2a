"""
Ce fichier répertorie l'ensemble des fonctions permettant le nettoyage de données textuelles.
"""
from settings import *
import logging
import swifter

logging.info("Load fr_core_news_md")
__NLP = spacy.load('fr_core_news_md')

logging.info("Load fr_core_news_sm")
__NLP_FR = fr_core_news_sm.load()

"""#outil pour stemming -racinisation des termes
__FS = SnowballStemmer("french")"""


def suppr_ponctuation(text):
    """
     Fonction supprimant les éléments de ponctuation dans une variable texte.
     Ici nous remplaçons les ponctuations !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~’ # symbole proche de l'apostrophe découvert dans la base de données
 par un espace sauf "_" et "-".
     :param text: la chaîne de caractère visée
     :return: la chaîne de caractère en question sans ponctuation
     """
    ponctuations = list(string.punctuation)
    ponctuations.remove("_")
    ponctuations.remove("-")
    ponctuations.append("’") # symbole proche de l'apostrophe découvert dans la base de données
    for ponctuation in ponctuations:
        text = text.replace(ponctuation, " ")
    text = "".join([mot for mot in list(text)])
    return text

def minuscule(text):
    """
    Fonction qui passe une variable texte en minuscule pour éviter les problèmes de casse.
    :param text : chaîne de caractères visée
    :return: la chaîne de caractère sans les majuscules
    """
    return text.lower()


def tokenization(text):
    """
    Fonction qui permet de récupérer un texte sous forme de liste de mots. Exemple: "Il fait beau" devient ['Il', 'fait', 'beau']
    :param text: texte visé
    :return: liste de mots
    """
    # transformer les messages en listes par tokenisation
    text = word_tokenize(text)
    return text

def suppr_stopwords(text_tokenized):
    """
    Fonction qui permet de supprimer les mots courants sans perte de sens. Exemple: ces mots sont généralement des déterminants,
    conjonctions, pronoms conjugaison des verbes être et avoir etc.
    :param text_tokenized: texte préalabelement tokenizé (liste de mots)
    :return: texte tokenizé sans les stopwords (liste de mots)
    """
    #chargement des stopwords
    mots_vides = stopwords.words('french')
    #retirer les stopwords et reformer les messages
    text_tokenized = [w for w in text_tokenized if not w in mots_vides]
    return text_tokenized

def reconstruction_phrase(text_tokenized):
    """
    Fonction qui concatène les mots d'une liste en une phrase.
    :param text_tokenized: texte préalabelement tokenizé (liste de mots)
    :return: texte reconstitué
    """
    #reformer les messages à partir des listes
    return " ".join(text_tokenized)


# tokenization et lemmatisation avec spacy ; alternative à la fonction ci-dessus qui réalise du stemming (troncature).
def lemmatisation(texte):
    #tokenization
    doc = __NLP(texte)
    tokens = [x for x in doc]

    # retrait des stop words ; on se sert tt de meme de nltk
    # from nltk.corpus import stopwords
    """stopWords = set(stopwords.words('french'))
    # stopWords = stopwords.words('french')
    clean_mot = [mot for mot in texte if mot not in stopWords]"""

    #lemmatization ; je tente de lemmatiser en conservant les phrases ou sans conserver les phrases.
    #lemma_phrase = [phrase.lemma_ for phrase in doc.sents]       #lemma : str ; lemma_ : int ?
    lemma_mot = [mot.lemma_ for mot in tokens]

    return  lemma_mot


def apply_parallel(df_grouped, func):
    """
    Parallélise le le calcul des prix
    :param df_grouped: dataframe groupé
    :param func: fonction de recherche des doublons
    :return: Dataframe d'entrée avec les différents groupes données
    """
    with Pool(cpu_count()) as pool:
        ret_list = pool.map(func, [dataframe for name, dataframe in df_grouped])
    return pd.concat(ret_list)

def parallelize_dataframe(df, func):
    df_split = np.array_split(df, cpu_count())
    pool = Pool(cpu_count())
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df


def nettoyage(dataFrame):
    start_time1 = time.time()

    dataFrame[" Commentaire_nettoyé"] = dataFrame[" Commentaire"].apply(commentary_cleaning)
    return dataFrame


def swifter_clean(dataFrame):
    """This method must speed up the proccess. Does the same
    as nettoyage() but it use swifter (https://github.com/jmcarpenter2/swifter)
    to parallilize if needed. After multiple tests, it does not speed up
    the process :(. It seems that Swifter has some issues with strings. 

    :param dataFrame: a pandas dataframe with the corpus
    :type dataFrame: DataFrame
    :return: a pandas dataframe with cleaned commentary
    :rtype: DataFrame
    """
    start_time1 = time.time()

    dataFrame[" Commentaire_nettoyé"] = dataFrame[" Commentaire"].swifter.apply(commentary_cleaning)
    return dataFrame

def commentary_cleaning(a_line):
    """all the code to clean a commentary

    :param a_line: a commentary
    :type a_line: a cleaned commentary
    """
    lowered_line = minuscule(a_line)
    no_ponctuation = suppr_ponctuation(lowered_line)
    tokens= tokenization(no_ponctuation)
    tokens_without_sw = suppr_stopwords(tokens)
    rebuild_sentence = reconstruction_phrase(tokens_without_sw)
    lemned = lemmatisation(rebuild_sentence)
    return lemned



