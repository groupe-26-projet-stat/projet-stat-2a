## Package à installer
Avant de faire tourner le script, installer les packages nécessaires listés dans le fichier 'requirements.txt':
utiliser: 'pip install -r requirements.txt'.

L'utilisation de spacy recquiert d'utiliser python 3.7, ne fonctionne pas sous python 3.8.
Pour utiliser spacy, rentrer la ligne de commande suivante dans le terminal : 
'python -m spacy download fr_core_news_md'.

La version utilisée originellement pour spacy est python 3.7.2.