from settings import *
from scraping import *
from nettoyage import *

def to_tfidf(dataFrame):
    """

    :param dataFrame:
    :return:
    """
    parseur = TfidfVectorizer(min_df=0.025, max_df=0.975, ngram_range=(1, 3))  # min_df et max_df : minimum (et max) d'apparition d'un mot
    tfidf = parseur.fit_transform(dataFrame[" Commentaire_nettoyé"])
    mat=pd.DataFrame(tfidf.toarray(),index=dataFrame[" Commentaire_nettoyé"],columns=parseur.get_feature_names())
    print("\nAffichage de la matrice tfidf de {}\n".format(dataFrame["Fournisseur"][0]), mat)
    return tfidf, parseur.get_feature_names()

def topics_nmf(tfidf, no_topics):
    """

    :param tfidf:
    :param no_topics:
    :return:
    """
    nmf = NMF(n_components=no_topics, random_state=1, alpha=.1, l1_ratio=.5, init='nndsvd')
    nmf.fit(tfidf)
    return nmf

def display_topics(model, feature_names, no_top_words=10):
    """

    :param model:
    :param feature_names:
    :param no_top_words:
    :return:
    """
    composants = []
    print("\nLes composants des différents topics.")
    for topic_idx, topic in enumerate(model.components_):
        print("Topic {}:".format(topic_idx))
        print(" - ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]]))
        composants.append(" - ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]]))
    return composants

def attribution_topic(tfidf, model, composants,  dataFrame):
    """

    :param tfidf:
    :param model:
    :param composants:
    :param dataFrame:
    :return:
    """
    nmf_output = model.transform(tfidf[0])
    topicnames = ["Topic" + str(i) for i in range(model.n_components)]
    pd.set_option('display.max_columns', None)
    df_document_topic = pd.DataFrame(np.round(nmf_output, 2), columns=topicnames, index=dataFrame[" Commentaire_nettoyé"])
    dominant_topic = np.argmax(df_document_topic.values, axis=1)
    df_document_topic['dominant_topic'] = dominant_topic
    print("\nAttribution des topics aux commentaires (affichage des 8 premières lignes)\n", df_document_topic[:8])
    df=pd.DataFrame(dominant_topic, columns=[" Topic"])
    df[" Composants_topic"] = ""
    for topic in range(len(composants)):
        df.loc[df[df[" Topic"]==topic].index, " Composants_topic"] = composants[topic]
    return df

def synthese_topics(dataFrame, n_topic):
    """

    :param dataFrame:
    :return:
    """
    tfidf = to_tfidf(dataFrame)

    model = topics_nmf(tfidf[0], n_topic)
    composants = display_topics(model, tfidf[1])
    attribution = attribution_topic(tfidf, model, composants, dataFrame)

    new_dataFrame = pd.concat([dataFrame, attribution], axis=1)
    print("Voici la nouvelle dataFrame pour {}\n".format(dataFrame["Fournisseur"][0]), new_dataFrame)
    return new_dataFrame


def polarite_textblob(text):
    """

    :param text:
    :return:
    """

    polarite = TextBlob(text, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer()).sentiment[0]
    return polarite

nlp = spacy.load('fr_core_news_sm')
nlp.add_pipe('spacytextblob')

def polarite_SpaCy(text):
    """

    :param text:
    :return:
    """
    return nlp(text)._.polarity

sia = SentimentIntensityAnalyzer()

def polarite_Vader(text):
    """

    :param text:
    :return:
    """
    return sia.polarity_scores(text)['compound']

def comparaison_temps2(dataFrame, liste_n=[1,10, 100, 500]):
    temps1 = []
    temps2 = []
    for n in liste_n:
        start_time1 = time.time()
        df=pd.DataFrame(dataFrame.iloc[:n])
        print(df)
        df[" Polarite4"] = df[" Commentaire_nettoyé"].apply(lambda x: sia.polarity_scores(x)['compound'])
        end_time1 = time.time()
        temps1.append(end_time1 - start_time1)
        start_time2 = time.time()
        df[" Polarite4"] = df[" Commentaire_nettoyé"].apply(lambda x: sia.polarity_scores_max(x)['compound'])
        end_time2 = time.time()
        temps2.append(end_time2 - start_time2)
    print(temps1)
    print(temps2)
    plt.plot(liste_n, temps1, label="original")
    plt.plot(liste_n, temps2, label="max")
    plt.title("Comparaison de la vader sur commentaire original et nettoyé")
    plt.xlabel("Nombre de lignes")
    plt.ylabel("Temps en secondes")
    plt.legend()
    plt.show()


def n_topic_opti(L_dataframes):

    L_opti = []
    for csv in L_dataframes:
        df = creation_dataFrame("RepertoirePropre/" + csv)

        df["choix_topics"] = df[" Commentaire_nettoyé"].apply(tokenization)
        txt = df["choix_topics"]
        L =[]
        for i in range(len(txt)):
            L.append(txt.loc[i])
        # On crée le dictionnaire
        dictionary = Dictionary(L)
        # filtrage des valeurs extrêmes
        dictionary.filter_extremes(
            no_below=3,
            no_above=0.85,
            keep_n=5000
        )
        # On crée le format du bag of words (liste (token_id, token_count))
        corpus = [dictionary.doc2bow(text) for text in L]
        # On crée la liste des nombres de topics à tester
        topic_nums = list(np.arange(5, 50 + 1,1))
        # On run le modèle NMF et on calcule leur score de cohérence
        # Pour tous les nombres de topics qu'on veut tester
        coherence_scores = []

        for num in topic_nums:
            nmf = GensimNmf(
                corpus=corpus,
                num_topics=num,
                id2word=dictionary,
                minimum_probability=0.05,
                random_state=42
            )
            # On run le modèle de cohérence pour avoir le score
            cm = CoherenceModel(
                model=nmf,
                texts=list(df["choix_topics"]),
                corpus=dictionary,
                coherence='c_v'
            )
            coherence_scores.append([num, cm])

        # cm est un objet, on cherche à avoir le score via get_coherence
        for e in coherence_scores:
            e[1] = e[1].get_coherence()
        print('{} : '.format(coherence_scores))

        # On cherche le nb de topics qui maximise coherence_score
        n_topic_opti = coherence_scores[0][0]
        max_cm = coherence_scores[0][1]
        for e in coherence_scores:
            if e[1]>max_cm:
                max_cm = e[1]
                n_topic_opti = e[0]
        L_opti.append(n_topic_opti)

    return(L_opti)



### Code utilisé pour comparer les performances des packages pour l'analyse des sentiments

# Sample de test pour les trois librairies Vader, Spacy, TextBlob
# listeCSVPropre = os.listdir("RepertoirePropre")
# dataFramesPropres =[]
# for csv in listeCSVPropre:
#     dataFramesPropres.append(creation_dataFrame("RepertoirePropre/" + csv))
#
# df = pd.concat(dataFramesPropres, ignore_index = True)
#
# df_sample = df.sample(100)
# print(df_sample)
#
# df_sample.to_csv('sample_sentiment.csv', sep='|', index=False)
#
# df_ech = pd.read_csv(open('sample_sentiment.csv','r'), sep = ';', encoding='utf-8')

# sentiment_th = df_ech["Sentiment_theorique"]
# # print(sentiment_th)
#
# df_ech["Vader"] = df_ech[" Commentaire_nettoyÃ©"].apply(polarite_Vader)
# df_ech["SpaCy"] = df_ech[" Commentaire_nettoyÃ©"].apply(polarite_SpaCy)
# df_ech["TextBlob"] = df_ech[" Commentaire_nettoyÃ©"].apply(polarite_textblob)
#
# df_positif = df_ech[df_ech.Sentiment_theorique == 1].filter(["Vader", "SpaCy", "TextBlob"])
# df_negatif = df_ech[df_ech.Sentiment_theorique == -1].filter(["Vader", "SpaCy", "TextBlob"])
# df_insatisfait = df_ech[df_ech.Sentiment_theorique == -0.5].filter(["Vader", "SpaCy", "TextBlob"])
# df_satisfait = df_ech[df_ech.Sentiment_theorique == 0.5].filter(["Vader", "SpaCy", "TextBlob"])
#
# sns.boxplot(x="variable", y="value", data=pd.melt(df_positif))
# # plt.ylim([-1,1])
# plt.title("Avis très positif")
# plt.show()
# sns.boxplot(x="variable", y="value", data=pd.melt(df_negatif))
# plt.title("Avis très négatif")
# plt.show()
# sns.boxplot(x="variable", y="value", data=pd.melt(df_insatisfait))
# plt.title("Avis modéré négatif")
# plt.show()
# sns.boxplot(x="variable", y="value", data=pd.melt(df_satisfait))
# plt.title("Avis modéré positif")
# plt.show()

