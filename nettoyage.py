"""
Ce fichier répertorie l'ensemble des fonctions permettant le nettoyage de données textuelles.
"""
from settings import *

nlp_fr = fr_core_news_sm.load()
nlp = spacy.load('fr_core_news_md')

phrase_test = """Je rencontre un problème d'eau chaude depuis 15 min des fois 16h. Il a fallu 3 interventions de tecniciens différents
à chaque fois pour trouver le problème, bien qu'un technicien, le premier, l'avait déjà trouvé, il a été contredit par le second, pour finalement être confirmé par le troisième. C'est la 4ème fois. Résultat,
 mon problème d'eau chaude ne sera pas résolu avant le 22 Décembre 2020, soit pas d'eau chaude  pendant encore 15 jours, en plein hiver. ça m'a couté 70€ pour 50m2"""

def suppr_ponctuation(text):
    """
     Fonction supprimant les éléments de ponctuation dans une variable texte.
     Ici nous remplaçons les ponctuations !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~’ # symbole proche de l'apostrophe découvert
     dans la base de données par un espace sauf "_" et "-".
     :param text: la chaîne de caractère visée
     :return: la chaîne de caractère en question sans ponctuation
     """
    ponctuations = list(string.punctuation)
    ponctuations.remove("_")
    ponctuations.remove("-")
    ponctuations.append("’") # symbole proche de l'apostrophe découvert dans la base de données
    for ponctuation in ponctuations:
        text = text.replace(ponctuation, " ")
    text = "".join([mot for mot in list(text)])
    return text

def minuscule(text):
    """
    Fonction qui passe une variable texte en minuscule pour éviter les problèmes de casse.
    :param text : chaîne de caractères visée
    :return: la chaîne de caractère sans les majuscules
    """
    return text.lower()

def tokenization(text):
    """
    Fonction qui permet de récupérer un texte sous forme de liste de mots. Exemple: "Il fait beau" devient ['Il', 'fait', 'beau']
    :param text: texte visé
    :return: liste de mots
    """
    # transformer les messages en listes par tokenisation
    text = word_tokenize(text)
    return text

def suppr_stopwords(text_tokenized):
    """
    Fonction qui permet de supprimer les mots courants sans perte de sens. Exemple: ces mots sont généralement des déterminants,
    conjonctions, pronoms conjugaison des verbes être et avoir etc.
    :param text_tokenized: texte préalabelement tokenizé (liste de mots)
    :return: texte tokenizé sans les stopwords (liste de mots)
    """
    #chargement des stopwords
    mots_vides = stopwords.words('french')
    mots_vides.extend(["a", "engie", "éni", "eni", "edf", "enr", "edf-enr", "total", "direct", "énergie"])
    mots_vides.remove("pas")
    #retirer les stopwords et reformer les messages
    text_tokenized = [w for w in text_tokenized if not w in mots_vides]
    return text_tokenized

def reconstruction_phrase(text_tokenized):
    """
    Fonction qui concatène les mots d'une liste en une phrase.
    :param text_tokenized: texte préalabelement tokenizé (liste de mots)
    :return: texte reconstitué
    """
    #reformer les messages à partir des listes
    return " ".join(text_tokenized)

def sentence_pos_filter(sentence):  # May take few minutes
    """
    Fonction qui permet de lemmatiser une phrase c'est à dire ne garder que les noms, les adjectifs et _____ ceci en les
    mettant à leur racine. C'est à dire que l'on retire les accords.
    Exemple: "eau chaude" devient "eau chaud"
    :param sentence: text à lemmatiser
    :return: text lemmatisé
    """
    sentence_parsed = nlp_fr(sentence)
    # Filter on POS tags and StopWords
    sentence_words_filtered = [word.lemma_ for word in sentence_parsed if  # (
                               # ((
                               word.pos_ in ["NOUN", "ADJ", "PROPN"]
                               # Ici, c’est pour filter les composantes necessaire. Vous pouvez ajouter “VERB” et autres si vous voulez)
                               # or word.is_stop) and word.is_alpha)
                               ]
    sentence_filtered = ' '.join(sentence_words_filtered)
    return sentence_filtered

#print(sentence_pos_filter(remplace_nb_indetermine(remplace_ordre(remplace_date(remplace_superficie(remplace_euro(remplace_jour(remplace_minute(remplace_heure(suppr_ponctuation(minuscule(phrase_test))))))))))))
#print(reconstruction_phrase(suppr_stopwords(tokenization(phrase_test))))

def ligne_propre(text):
    """
    Fonction qui applique à un text l'ensemble des fonctions permettant le nettoyage dans l'ordre adéquat.
    :param text: text à nettoyer
    :return: text nettoyé
    """
    text = minuscule(text)
    print(text)
    text = suppr_ponctuation(text)
    print(text)
    text = tokenization(text)
    text = suppr_stopwords(text)
    text = reconstruction_phrase(text)
    print(text)
    text = sentence_pos_filter(text)
    print(text)
    return text

def nettoyage(dataFrame):
    """
    Fonction qui applique la fonction ligne_propre à l'ensemble des commentaire d'une dataFrame
    :param dataFrame: dataFrame d'origine
    :return: dataFrame avec une colonne supplémentaire contenant les commentaires nettoyés
    """
    dataFrame[" Commentaire_nettoyé"] = dataFrame[" Commentaire"].apply(ligne_propre)
    print(dataFrame[" Commentaire_nettoyé"][0], "\n", dataFrame[" Commentaire"][0], "\n")
    return dataFrame