"""
Ce fichier regroupe les fonctions permettant la récupération des données pour notre étude statistique
"""
from settings import *


dict_fournisseurs = {"eni":"fr.eni.com", "total": "total.direct-energie.com", "engie":"engie-homeservices.fr", "edf":"edfenr.com"}

def convert_emojis(text):
    """
    Fonction pour convertir les emojis en mots
    :param text: texte contenant des emojis à convertir
    :return text: texte avec traduction des emojis
    """
    for emot in UNICODE_EMO:
        # emot_space = " " + emot
        # text.replace(emot, emot_space)
        # i = text.find(UNICODE_EMO[emot])
        # text = text[:i] + " " + text[i:]

        # Code original
        # text = text.replace(emot, "_".join(UNICODE_EMO[emot].replace(",","").replace(":","").split()))

        #ajoute un espace au debut et a la fin ; les autres manipulations me semblent inutiles
        text = text.replace(emot, UNICODE_EMO[emot].replace(":", " "))
    return text


def convert_emoticons(text):
    """
    Fonction pour convertir les emoticons en mots
    :param text: texte contenant des emoticons à convertir
    :return text: texte avec traduction des emoticons
    """
    for emot in EMOTICONS:
        text = re.sub(u'('+emot+')', " " + "_".join(EMOTICONS[emot].replace(",","").split()) + " ", text)
        #text = re.sub(u'('+ emot +')', " " + EMOTICONS[emot].replace(",", " ") + " ", text)
    return text

# Exemples
"""text1 = "Hilarious😂. The feeling of making a sale 😎, The feeling of actually fulfilling orders 😒"
print(convert_emojis(text1))

text = "Hello:-) :-)"
print(convert_emoticons(text))"""


def scraping_to_csv(fournisseur):
    """
    Fonction pour scraper les avis clients d'un fournisseur sur le site Avis-verifiés.
    Ce fournisseur doit être répertorié dans le dictionnaire dict_fournisseurs dont les clefs sont les noms des
    fournisseurs et les valeurs sont les particules de l'url qui leurs sont propres. Exemple pour Eni, l'url est
    "https://www.avis-verifies.com/avis-clients/fr.eni.com" et la particule est "fr.eni.com". La fonction enregistre un
    fichier du type "fournisseur_date.csv" dans le répertoire en cours. Ce csv contient les informations : Fournisseur|
    Note| Date_du_poste| Date_experience| Commentaire| Reponse à la date du jour. Le separateur utilisé sera
    "|" (alt gr + 6).
    :param fournisseur: nom du fournisseur dont les commentaires vont être scrapés (clef de dict_fournisseurs !!!)
    """
    today = date.today()
    date_scrap = today.strftime("%b-%d-%Y")
    with open('RepertoireCSV/'+ fournisseur + '_' + date_scrap + '.csv','w', encoding='utf-8') as outf :
        outf.write("Fournisseur| Note| Date_du_poste| Date_experience| Commentaire| Reponse\n")
        url = "https://www.avis-verifies.com/avis-clients/{}".format(dict_fournisseurs[fournisseur])
        reponse = requests.get(url)
        if reponse.ok:
            soup = BeautifulSoup(reponse.text, "html.parser")
            select = soup.find("select")
            nbpages = str.split(select.text)
            nbpages = int(nbpages[-1])
            print(nbpages)

        start_for = time.time()
        t = 0
        for i in range(1, nbpages + 1):
            t = round(time.time() - start_for, 2) # temps depuis debut de la boucle pour chq nouvelle page scrapee
            print('page {}, {}sec'.format(i, t))
            url = 'https://www.avis-verifies.com/avis-clients/{}?filtre=&p={}'.format(dict_fournisseurs[fournisseur], str(i))
            response = requests.get(url)
            if response.ok :
                soup = BeautifulSoup(response.text, "html.parser")
                #On va chercher la partie du code html correspondant aux commentaires et réponse s'il y a
                #pour chaque personne
                comments = soup.find_all('div', {'class':'review row'})
                #print(comments)
                for comment in comments:
                    bol=False
                    notes = comment.find('p' , {'class':'rating'}).find('span',{'itemprop':'ratingValue'})
                    notebis = notes.text
                    #dateposteb = date du poste et dateexp= date de l'expérience
                    dateposteb = comment.find('meta',{'itemprop' : 'datePublished'})
                    dateposte = dateposteb['content']
                    dateexpb = comment.find('div',{'class' : 'details suite'})
                    da=dateexpb.text
                    #Partie a améliorer! j'ai trouvé cette manière de faire mais il y en a sans doute une plus
                    #performente
                    dab=da.replace('suite à une expérience du ','')
                    dateexp = dab[0:10]
                    commentaireb=comment.find('div',{'class':'comment'}).find('div',{'class':'text'})
                    #On retire les espaces puis les sauts de ligne et tabulation
                    commentairec=commentaireb.text.lstrip().rstrip()
                    t = re.compile(r'[\n\r\t]')
                    commentaire = t.sub(" ", commentairec)
                    #Partie a modifier !! On essaye ensuite de retirer les smiley/ou de les afficher
                    commentaire = convert_emojis(commentaire)
                    commentaire = convert_emoticons(commentaire)
                    """myre = re.compile(u'['
                                                u'\U0001F300-\U0001F64F'
                                                u'\U0001F680-\U0001F6FF'
                                                u'\u2600-\u26FF\u2700-\u27BF]+',
                                                re.UNICODE)
                    commentaire = myre.sub(" ", commentaire)"""
                    #On va essayer de regarder s'il y a une réponse au commentaire s'il y a on ajoute le
                    #commentaire a la BDD sinon on mets un Nan
                    try :
                        reponseb = comment.find('div',{'class':'merchant'}).find('div',{'class':'text'})
                        bol=True
                    except AttributeError:
                        bol=False
                    if bol :
                        reponseq = reponseb.text.lstrip().rstrip()
                        t=re.compile(r'[\n\r\t]')
                        reponseq=t.sub(" ",reponseq)
                    else :
                        reponseq='NaN'
                    outf.write(fournisseur + '|' + str(notebis) + '|' + str(dateposte) + '|' + str(dateexp) + '|' + str(commentaire) + '|' + str(reponseq) + '\n')
                    del notes

def scraping_to_csv2(fournisseur, maxdate):
    """

    :param fournisseur:
    :param maxdate:
    :return:
    """
    today = date.today()
    date_scrap = today.strftime("%b-%d-%Y")
    with open('RepertoireCSV/'+fournisseur + '_' + date_scrap + '.csv', 'w', encoding='utf-8') as outf:
        outf.write("Fournisseur| Note| Date_du_poste| Date_experience| Commentaire| Reponse\n")
        url = "https://www.avis-verifies.com/avis-clients/{}".format(dict_fournisseurs[fournisseur])
        reponse = requests.get(url)
        if reponse.ok:
            soup = BeautifulSoup(reponse.text, "html.parser")
            select = soup.find("select")
            nbpages = str.split(select.text)
            nbpages = int(nbpages[-1])
            print(nbpages)
        booleen=True
        for i in range(1, nbpages + 1):
            if booleen:
                print("numéros de page : "+str(i))
                url = 'https://www.avis-verifies.com/avis-clients/{}?filtre=&p={}'.format(
                    dict_fournisseurs[fournisseur], str(i))
                response = requests.get(url)
                if response.ok:
                    soup = BeautifulSoup(response.text, "html.parser")
                    # On va chercher la partie du code html correspondant aux commentaires et réponse s'il y a
                    # pour chaque personne
                    comments = soup.find_all('div', {'class': 'review row'})
                    # print(comments)
                    for comment in comments:
                        notes = comment.find('p', {'class': 'rating'}).find('span', {'itemprop': 'ratingValue'})
                        notebis = notes.text
                        # dateposteb = date du poste et dateexp= date de l'expérience
                        dateposteb = comment.find('meta', {'itemprop': 'datePublished'})
                        dateposte = dateposteb['content']
                        if datetime.strptime(dateposte, "%d/%m/%Y") < maxdate:
                            booleen = False
                            break
                        dateexpb = comment.find('div', {'class': 'details suite'})
                        da = dateexpb.text
                        # Partie a améliorer! j'ai trouvé cette manière de faire mais il y en a sans doute une plus
                        # performente
                        dab = da.replace('suite à une expérience du ', '')
                        dateexp = dab[0:10]
                        commentaireb = comment.find('div', {'class': 'comment'}).find('div', {'class': 'text'})
                        # On retire les espaces puis les sauts de ligne et tabulation
                        commentairec = commentaireb.text.lstrip().rstrip()
                        t = re.compile(r'[\n\r\t]')
                        commentaire = t.sub(" ", commentairec)
                        # Partie a modifier !! On essaye ensuite de retirer les smiley/ou de les afficher
                        commentaire = convert_emojis(commentaire)
                        commentaire = convert_emoticons(commentaire)
                        """myre = re.compile(u'['
                                                    u'\U0001F300-\U0001F64F'
                                                    u'\U0001F680-\U0001F6FF'
                                                    u'\u2600-\u26FF\u2700-\u27BF]+',
                                                    re.UNICODE)
                        commentaire = myre.sub(" ", commentaire)"""
                        # On va essayer de regarder s'il y a une réponse au commentaire s'il y a on ajoute le
                        # commentaire a la BDD sinon on mets un Nan
                        try:
                            reponseb = comment.find('div', {'class': 'merchant'}).find('div', {'class': 'text'})
                            bol = True
                        except AttributeError:
                            bol = False
                        if bol:
                            reponseq = reponseb.text.lstrip().rstrip()
                            t = re.compile(r'[\n\r\t]')
                            reponseq = t.sub(" ", reponseq)
                        else:
                            reponseq = 'NaN'
                        outf.write(
                            fournisseur + '|' + str(notebis) + '|' + str(dateposte) + '|' + str(dateexp) + '|' + str(commentaire) + '|' + str(
                                reponseq) + '\n')
                        del notes


def creation_dataFrame(csv):
    """
    Fonction pour transformer le fichier csv mis en entrée en une dataFrame pandas. Elle change aussi le type des
    variables "Date_du_poste" et "Date_experience" en format date.
    :param csv: csv à transformer en dataFrame
    :return: dataFrame correspondant
    """
    df = pd.read_csv(csv, delimiter="|", header = 0)
    df[" Date_du_poste"] = pd.to_datetime(df[" Date_du_poste"], dayfirst=True)
    df[" Date_experience"] = pd.to_datetime(df[" Date_experience"], dayfirst=True)
    df[" Delai"] = df[" Date_du_poste"] - df[" Date_experience"]
    df[" ReponseFlag"] = 0
    df.loc[df[pd.isna(df[" Reponse"]) == False].index, " ReponseFlag"] = 1
    df[" ReponseFlag"] = pd.Categorical(df[" ReponseFlag"])
    df.loc[df[pd.isna(df[" Commentaire"]) == True].index, " Commentaire"] = " "
    try :
        df.loc[df[pd.isna(df[" Commentaire_nettoyé"]) == True].index, " Commentaire_nettoyé"] = " "
    except:
        pass
    return(df)


'''
print(len(notebis))
print(len(dateposte))
print(len(dateexp))
print(len(commentaire))
print(len(reponseq))
'''
