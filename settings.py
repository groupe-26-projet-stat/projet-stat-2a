"""
Ce fichier comporte tous les modules pyhton utilisés pour la réalisation de cette étude statistique.
"""

"""Modules ne nécissantant PAS d'installation préalable """
import re
import time
from datetime import date, datetime, timedelta
import string
from dateutil.relativedelta import relativedelta
import os
from multiprocessing import Pool, cpu_count, freeze_support

"""Modules nécissantant une installation préalable """
import requests
from bs4 import BeautifulSoup
from emot.emo_unicode import UNICODE_EMO, EMOTICONS

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as stats
import nltk

#Pour le premier run, enlevez le # ci-dessous et installer avec l'interface "stopwords" et "punkt"
#nltk.download()

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF

import spacy
import fr_core_news_md
import fr_core_news_sm

# import model
from vaderSentiment_fr.vaderSentiment import SentimentIntensityAnalyzer

from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer

from spacytextblob.spacytextblob import SpacyTextBlob

#Pour la méthode calculant le nombre de topics optimal
from gensim.corpora import Dictionary
from gensim.models.coherencemodel import CoherenceModel
from gensim.models.nmf import Nmf as GensimNmf