from settings import *
from scraping import *
from exploration import exploration
from text_mining import *
from nettoyage import *


listeCSV = os.listdir("RepertoireCSV")

################### Scraping et Nettoyage des données textuelles #######################################################

question1 = ""
i=0
while question1 not in ["Oui", "oui", "Non", "non"]:
    if i != 0:
        print("Merci de répondre par Oui ou par Non !")
    i += 1
    question1 = input("Voulez vous réinitialiser la base de données, i.e constuire une nouvelle base ? (Oui/Non): ")
    if question1 in ["Oui", "oui"]:
        print("Scraping en cours, cela peut prendre du temps !")
        for csv in listeCSV:
            os.remove('RepertoireCSV/'+csv)
        scraping_to_csv("eni")
        scraping_to_csv("total")
        scraping_to_csv("engie")
        scraping_to_csv("edf")
        listeCSV = os.listdir("RepertoireCSV")
        dataFrames = []
        for csv in listeCSV:
            dataFrames.append(creation_dataFrame('RepertoireCSV/' + csv))
        print("\nNettoyage des données textuelles\n")
        for dataFrame in dataFrames:
            print(time.time())
            df = nettoyage(dataFrame)
            df.to_csv('RepertoirePropre/'+dataFrame["Fournisseur"][0] + '.csv', sep='|', index=False)
        print(time.time())

    elif question1 in ["Non", "non"]:
        print("Vous avez décidé de ne pas réinitialiser la base de données !\n")
        question2 = ""
        i=0
        while question2 not in ["Oui", "oui", "Non", "non"]:
            if i != 0:
                print("Merci de répondre par Oui ou par Non !")
            i += 1
            question2 = input("Voulez vous de nouveau scraper la base de données, i.e. rajouter les dernier commentaire publiés sur le site ? (Oui/Non): ")
            if question2 in ["Oui", "oui"]:
                print("Scraping en cours, cela peut prendre du temps !")
                noms = []
                dates_max = []
                dataFrames = []
                today = date.today()
                date_scrap = today.strftime("%b-%d-%Y")
                for csv in listeCSV:
                    print('1'+csv)
                    df = creation_dataFrame('RepertoireCSV/'+csv)
                    df[" Date_du_poste"] = pd.to_datetime(df[" Date_du_poste"], dayfirst=True)
                    maximum = df[" Date_du_poste"].max()
                    dates_max.append(maximum)
                    noms.append(csv.split("_")[0])
                    dataFrames.append(df)
                    os.remove('RepertoireCSV/' + csv)
                for nom, date in zip(noms, dates_max):
                    print('scrap'+nom+'date : '+str(date))
                    scraping_to_csv2(nom, date)
                listeCSV = os.listdir("RepertoireCSV")
                dataFrames2 = []
                for csv in listeCSV:
                    print('2'+csv)
                    dataFrames2.append(creation_dataFrame('RepertoireCSV/'+csv))
                    os.remove('RepertoireCSV/' + csv)
                for dataFrame1, dataFrame2 in zip(dataFrames, dataFrames2):
                    df = pd.concat([dataFrame1, dataFrame2])
                    #df[" Delai"] = df[" Delai"].astype(str)
                    #df[" Date_du_poste"]=df[" Date_du_poste"].astype(str)
                    #df[" Date_experience"] = df[" Date_experience"].astype(str)
                    fournisseur = 'RepertoireCSV/' + df.iloc[0,0] + '_' + date_scrap + '.csv'
                    print(fournisseur)
                    df.to_csv(fournisseur, sep='|', index=False)
                listeCSVPropre = os.listdir("RepertoirePropre")
                dataFrames_propre = []
                for csv in listeCSVPropre:
                    print('3'+csv)
                    dataFrames_propre.append(creation_dataFrame('RepertoirePropre/'+csv))
                    os.remove('RepertoirePropre/' + csv)
                print("\nNettoyage des données textuelles\n")
                dataFrames_propre2 = []
                for dataFrame in dataFrames2:
                    print(time.time())
                    dataFrames_propre2.append(nettoyage(dataFrame))
                    print(time.time())
                for dataFrame1, dataFrame2 in zip(dataFrames_propre, dataFrames_propre2):
                    df = pd.concat([dataFrame1, dataFrame2])
                    df.to_csv('RepertoirePropre/' + df.iloc[0,0] + '.csv', sep='|', index=False)

            elif question2 in ["Non", "non"]:
                print("Vous avez décidé de ne pas scraper de nouveau la base de données !\n")


################################### Stats Descs ########################################################################

listeCSVPropre = os.listdir("RepertoirePropre")
dataFramesPropres =[]
for csv in listeCSVPropre:
    dataFramesPropres.append(creation_dataFrame("RepertoirePropre/" + csv))
colors = ["#eb500a", "#0aa5eb", "#faca00", "#ec312e"]

dataFrames_Flags = exploration(dataFramesPropres, colors)

################################# Analyse des topics ###################################################################


# Liste du nombre de topics optimal pour chacun des fournisseurs de dataFrames_Flags, le 22/04/2021
Liste_nb_topics = [5, 35, 22, 20]
# Pour recalculer le nombre de topic optimal, lancer les 4 lignes suivantes : (! besoin d'une forte puissance de calcul)
# listeCSVPropre = os.listdir("RepertoirePropre")
# if __name__ == "__main__":
#   freeze_support()
#   Liste_nb_topics = n_topic_opti(listeCSVPropre)
# print(Liste_nb_topics)


dataFrames_topics = []
for i in range(len(Liste_nb_topics)):
    dataFrames_topics.append(synthese_topics(dataFrames_Flags[i], Liste_nb_topics[i]))


############################### Analyse des sentiments ##################################################################

dataFrames_polarites = []
for dataFrame in dataFrames_topics:
    dataFrame[" Polarite"] = dataFrame[" Commentaire_nettoyé"].apply(polarite_Vader)
    dataFrames_polarites.append(dataFrame)


############################# Mise en csv pour powerBI #################################################################

df = pd.concat(dataFrames_polarites)
df.to_csv('PowerBI.csv', sep='|', index=False)


