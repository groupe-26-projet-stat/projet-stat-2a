"""
Ce fichier contient des fonctions permettant d'effectuer une première exploration des données des founrisseurs
"""
from settings import *
from nettoyage import *

"""df_eni = creation_dataFrame("eni_Feb-01-2021.csv")
df_total = creation_dataFrame("total_Feb-08-2021.csv")
df_edf = creation_dataFrame("edf_Feb-07-2021.csv")
df_engie = creation_dataFrame("engie_Feb-06-2021.csv")

dataFrames = [df_eni, df_total, df_edf, df_engie]
colors = ["#faca00", "#ec312e", "#eb500a", "#0aa5eb"]"""


def detect_anomalieDate(dataFrames, CGU2020 = True, affichage = True):
    """
    Fonction qui permet de référencer les anomalies de dates. Elle crée une nouvelle variable AnomalieFlag qui prend la
    valeur 1 lorsque le commentaire contient une anomalie de date et 0 sinon. On parle d'anomalie de date lorsque la date de
    publication du commentaire est plus récente que la date d'experience client. L'option CGU2020 permet référencer ou non
    les commentaires qui ne respectent pas le délai de publication selon les conditions générales d'utilisation du site,
    soit 16 mois d'écart maximum entre la publication et l'experience client. L'option affichage permet de choisir ou non l'affichage
    de la nouvelle dataFrame dans la console.
    :param dataFrames: Liste de dataFrames
    :param CGU2020: Booléen
    :param affichage: Booléen
    :return: Liste de dataFrames avec la nouvelle variable AnomalieFlag
    """
    dataFrames_flaguees = []
    array = []
    index = []
    for dataFrame in dataFrames:
        dataFrame[" AnomalieFlag"] = 0
        dataFrame.loc[dataFrame[(dataFrame[" Delai"] / np.timedelta64(1, 'D')).astype(int) < 0].index, " AnomalieFlag"] = 1
        #Les avis sont publiés et conservés pendant une durée de cinq ans
        if CGU2020 == True:
            #D'après CGU Version 4.0 du 27/01/2020, 15mois entre l'experience et la publication
            dataFrame.loc[dataFrame[(dataFrame[" Delai"] / np.timedelta64(1, 'M')).astype(int) > 16].index, " AnomalieFlag"] = 1
        dataFrame[" AnomalieFlag"] = pd.Categorical(dataFrame[" AnomalieFlag"])
        dataFrames_flaguees.append(dataFrame)
        if affichage == True:
            index.append(dataFrame["Fournisseur"][0])
            stats = [len(dataFrame[dataFrame[" AnomalieFlag"] == 1]),
                     len(dataFrame[" AnomalieFlag"]),
                     round(len(dataFrame[dataFrame[" AnomalieFlag"] == 1])/len(dataFrame[" AnomalieFlag"])*100, 2)]
            array.append(stats)
    if affichage == True:
        df = pd.DataFrame(array, index=index, columns=['nb anomalies', 'nb commentaires', "taux d'anomalie' (%)"])
        print("Statistiques descriptive des anomalies des fournisseurs\n", df)
    return dataFrames_flaguees

def statDesc_Note(dataFrames, affichage = True):
    """
    Fonction qui permet de sortir les principales statistiques descriptives lié à la note. Elle comporte une option affichage
    qui permet de choisir ou non l'affichage des sorties dans la console.
    :param dataFrames: Liste de dataFrames à analyser
    :param affichage: Booléen
    :return: dataFrame récapitulant les statistiques correspondant aux notes des fournisseurs
    """
    array = []
    index = []
    for dataFrame in dataFrames:
        index.append(dataFrame["Fournisseur"][0])
        stats = [round(dataFrame[" Note"].describe()["count"]),
                 round(dataFrame[" Note"].describe()["mean"], 2),
                 round(dataFrame[" Note"].describe()["std"], 2),
                 round(dataFrame[" Note"].describe()["min"], 2),
                 round(dataFrame[" Note"].describe()["25%"], 2),
                 round(dataFrame[" Note"].describe()["50%"], 2),
                 round(dataFrame[" Note"].describe()["75%"], 2),
                 round(dataFrame[" Note"].describe()["max"], 2)]
        array.append(stats)
    df = pd.DataFrame(array, index = index, columns = ['count', 'mean', 'std', 'min', '25%', '50%', '75%', 'max'])
    if affichage == True:
        print("Statistiques descriptive des notes des fournisseurs\n", df)
    return df

def barplot_note(dataFrames, colors=None):
    """
    Fonction qui permet de visualier la répartitions des notes de chaque fournisseur sous la forme de barplot. L'option
    colors permet de spécifier sa propre palette de couleurs.
    :param dataFrames: Liste de dataFrames à analyser
    :param colors: Liste de couleurs associées à une dataFrame
    :return: affiche chaque barplot
    """
    if colors != None:
        for dataFrame, color in zip(dataFrames, colors):
            ax = sns.countplot(x=" Note", data=dataFrame, color=color)
            plt.title('Répartition des notes {}'.format(dataFrame["Fournisseur"][0]))
            plt.xlabel('notes obtenues')
            plt.ylabel("nombre de commentaires")
            total = len(dataFrame[' Note'])
            for p in ax.patches:
                percentage = '{:.1f}%'.format(100 * p.get_height() / total)
                x = p.get_x() + p.get_width() / 5
                y = p.get_y() + p.get_height() + 10
                ax.annotate(percentage, (x, y))
            plt.show()
    else:
        for dataFrame in dataFrames:
            ax = sns.countplot(x=" Note", data=dataFrame)
            plt.title('Répartition des notes {}'.format(dataFrame["Fournisseur"][0]))
            plt.xlabel('notes obtenues')
            plt.ylabel("nombre de commentaires")
            total = len(dataFrame[' Note'])
            for p in ax.patches:
                percentage = '{:.1f}%'.format(100 * p.get_height() / total)
                x = p.get_x() + p.get_width() / 5
                y = p.get_y() + p.get_height() + 10
                ax.annotate(percentage, (x, y))
            plt.show()

def boxplot_note(dataFrames, colors=None):
    """
    Fonction qui permet de comparer visuellement la répartition des notes de chaque fournisseur à l'aide d'un boxplot.
    L'option colors permet de choisir sa propre palette de couleurs.
    :param dataFrames: Liste de dataFrames à analyser
    :param colors: Liste de couleurs associées à une dataFrame
    :return: affiche le boxplot
    """
    df = pd.concat(dataFrames)
    if colors != None:
        palette = {}
        for dataFrame, color in zip(dataFrames, colors):
            palette[dataFrame["Fournisseur"][0]] = color
        ax = sns.boxplot(x="Fournisseur", y=" Note", data=df,
                         palette=palette)
        plt.title('Boxplot des notes de chacun des fournisseurs')
        plt.show()
    else:
        ax = sns.boxplot(x="Fournisseur", y=" Note", data=df)
        plt.title('Boxplot des notes de chacun des fournisseurs')
        plt.show()

def statDesc_Reponse(dataFrames, affichage = True):
    """
    Fonction qui permet de sortir les principales statistiques descriptives lié à la réponse. Elle comporte une option affichage
    qui permet de choisir ou non l'affichage des sorties dans la console.
    :param dataFrames: Liste de dataFrames à analyser
    :param affichage: Booléen
    :return: dataFrame récapitulant les statistiques correspondant aux réponses des fournisseurs
    """
    array = []
    index = []
    for dataFrame in dataFrames:
        index.append(dataFrame["Fournisseur"][0])
        stats = [len(dataFrame[dataFrame[" ReponseFlag"] == 1]),
                 len(dataFrame[" ReponseFlag"]),
                 round(len(dataFrame[dataFrame[" ReponseFlag"] == 1])/len(dataFrame[" ReponseFlag"])*100, 2)]
        array.append(stats)
    df = pd.DataFrame(array, index=index, columns=['nb reponse', 'nb commentaires', 'taux de réponse (%)'])
    if affichage == True:
        print("Statistiques descriptive des réponses des fournisseurs\n", df)
    return df

def testAnova_NoteReponse(dataFrames, affichage = True):
    """
    Fonction qui donne les résultats pour les différentes dataframes à un test de corrélation ANOVA (variable quantitative : la note,
    et variable qualitative : la présence d'une réponse du fournisseur. L'optio affichage permet de choisir ou non
    l'affichage des sorties dans la console.
    :param dataFrames: Liste de dataFrames à analyser
    :param affichage: Booléen
    :return: dataFrame récapitulant les statistiques correspondant aux tests ANOVA
    """
    array = []
    index = []
    for dataFrame in dataFrames:
        if len(dataFrame[dataFrame[" ReponseFlag"] == 1][" Note"]) != 0:
            fvalue, pvalue = stats.f_oneway(dataFrame[dataFrame[" ReponseFlag"] == 0][" Note"], dataFrame[dataFrame[" ReponseFlag"] == 1][" Note"])
            if affichage == True:
                print("Test de correlation entre les réponses fournisseurs et les notes pour {}\n".format(dataFrame["Fournisseur"][0]),
                        "F =", round(fvalue, 2), "- pvalue =", round(pvalue, 2))
            array.append([round(fvalue, 2), round(pvalue, 2)])
            index.append(dataFrame["Fournisseur"][0])
    df = pd.DataFrame(array, index=index, columns=['fvalue', 'pvalue'])
    return df

def boxplot_NoteReponse(dataFrames, colors=None):
    """
    Fonction qui permet de comparer visuellement la répartition des notes de chaque fournisseur en fonction de la présence
     d'une réponse ou non au commentaire à l'aide d'un boxplot. L'option colors permet de choisir sa propre palette de couleurs.
    :param dataFrames: Liste de dataFrames à analyser
    :param colors: Liste de couleurs associées à une dataFrame
    :return: affiche le boxplot
    """
    df = pd.concat(dataFrames)
    ax = sns.boxplot(x="Fournisseur", y=" Note", hue=" ReponseFlag", data=df)
    if colors != None:
        for i, color in zip(range(0, len(ax.artists), 2), colors):
            mybox = ax.artists[i]
            mybox.set_edgecolor(color)
            mybox.set_facecolor("white")
            mybox.set_linewidth(3)
            try:
                mybox = ax.artists[i+1]
                mybox.set_facecolor(color)
            except:
               pass
    plt.legend(title="Réponse présente", labels=['Oui', 'Non'], handles=[ax.artists[1], ax.artists[0]])
    plt.title("Boxplot des notes en fonction des réponses des fournisseurs aux commentaires")
    plt.show()

def unicite_reponse(dataFrames, affichage = True, unique = False):
    """
    Fonction qui permet de caractériser la personnalisation des réponses des fournisseurs. On a donc le nombre de réponses
    différentes via "réponses uniques", le nombre d'apparition de la top réponses via "top réponse", le total des réponses
    via "total réponse et la part de la top réponse parmi les réponses via "proportion top réponse". L'option affichage
    permet de choisir ou non l'affichage des sorties dans la console. L'option unique permet de choisir si 'lon souhaite stocker
    les différentes réponses des fournisseurs.
    :param dataFrames: Liste des dataFrames à analyser
    :param affichage: Booléen
    :param unique: Booléen
    :return: dataFrame des caractéristiques des réponses des fournisseurs, si option unique, dictionnaire des différentes réponses
    """
    array = []
    index = []
    dict_RepUniques = {}
    for dataFrame in dataFrames:
        try:
            dataFrame.loc[dataFrame[" ReponseFlag"]==1," Reponse"]= dataFrame[dataFrame[" ReponseFlag"]==1][" Reponse"].apply(suppr_ponctuation)
            dataFrame.loc[dataFrame[" ReponseFlag"]==1," Reponse"]= dataFrame[dataFrame[" ReponseFlag"]==1][" Reponse"].apply(minuscule)
            dataFrame.loc[dataFrame[" ReponseFlag"]==1," Reponse"]= dataFrame[dataFrame[" ReponseFlag"]==1][" Reponse"].apply(tokenization)
            dataFrame.loc[dataFrame[" ReponseFlag"]==1," Reponse"]= dataFrame[dataFrame[" ReponseFlag"]==1][" Reponse"].apply(reconstruction_phrase)
            array.append([dataFrame[" Reponse"].describe()["unique"], dataFrame[" Reponse"].describe()["freq"], dataFrame[" Reponse"].describe()["count"], round(dataFrame[" Reponse"].describe()["freq"]/dataFrame[" Reponse"].describe()["count"]*100, 2)])
            index.append(dataFrame["Fournisseur"][0])
            if unique == True:
                dict_RepUniques[dataFrame["Fournisseur"][0]] = dataFrame[" Reponse"].unique()
        except:
            pass
    pd.set_option('display.max_columns', None)
    df = pd.DataFrame(array, index=index, columns=["réponses uniques", "top réponse", "total réponses", "proportion top réponse (%)"])
    if affichage == True:
        print("Caractéristiques des réponses des fournisseurs\n", df)
    if unique == True:
        if affichage == True:
            print("Réponse uniques répertoriées\n", dict_RepUniques)
        return df, dict_RepUniques
    return df

def exploration(dataFrames, colors):
    """
    Fonction qui utilise toutes les fonctions précédentes utiles à l'analyse univariée de la base de données
    :param dataFrames: Liste de dataFrames
    :param colors: Liste de couleurs pour personnaliser l'affichage
    :return: None
    """
    dataFrames = detect_anomalieDate(dataFrames, CGU2020=True)
    print('\n')
    stat_notes = statDesc_Note(dataFrames)
    print('\n')
    barplot_note(dataFrames, colors)
    boxplot_note(dataFrames, colors)

    stat_reponses = statDesc_Reponse(dataFrames)
    print('\n')
    test = testAnova_NoteReponse(dataFrames)
    print('\n')
    boxplot_NoteReponse(dataFrames)

    reponseUnique = unicite_reponse(dataFrames)
    return dataFrames